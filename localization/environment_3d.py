#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
This file defines the structure of the simulation described in
main.py for the localization of multiple point-sources in 3-D space.

Copyright © 2017 by Dhruv Ilesh Shah
shahdhruv@cmu.edu | dhruv.shah@iitb.ac.in
Robotics Institute, Carnegie Mellon University
Indian Institute of Technology, Bombay
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.stats import poisson
from math import *
from copy import *

grid_size = 10.  # Dimension of the square(2D) grid
fusion_range = np.sqrt(3)  # Range of the sensor
sources = []  # A list of all sources
sensors = []  # A list of all sensors
particles = []  # A list of all particles
particle_population = 100
clusters = []  # List of clusters
cluster_centroids = []  # List of cluster centroids
k_read = 1  # Constant involved in the sensor reading term
known_sources = []  # Store known sources, for the sequential approach
bg_thresh = 0.5  # A measure of background/stray radiation at complete localization
time_steps = 5 # Number of time steps / iterations


class particle():

    def __init__(self, strength, xpos, ypos, zpos, weight):
        """
        Creates a particle with given parameters.
        Strength assumed in micro-Curie.
        """
        self._strength = np.array(strength, dtype=np.float64)
        self._position = np.array([xpos, ypos, zpos], dtype=np.float64)
        self._weight = np.array(weight, dtype=np.float64)
        self._id = len(particles) + 1

    def get_position(self):
        """
        Returns position of the particle.
        """
        return self._position

    def get_distance(self, position):
        """
        Returns Euclidean distance to particle.
        """
        return np.linalg.norm(self._position - position)

    def influence(self, position):
        """
        The strength of the particle measured with the 
        inverse-square assumptions, avoiding poles.
        """
        return 2. * np.array(self._strength) / (1e-4 + np.square(self.get_distance(position)))

    def update_weight(self, factor):
        """
        Updates weight of the particle by factor, and returns
        the updated weight
        """
        self._weight *= factor
        return self._weight

    def get_id(self):
        """
        Returns the ID of the particle.
        """
        return self._id

    def get_strength(self):
        """
        Returns the strength of the particle.
        """
        return self._strength

    def add_noise(self):
        """
        Adds Gaussian noise to the position parameters to allow variety.
        """
        self._position += np.random.normal(0.0, 0.2, np.shape(self._position))
        self._strength += np.random.normal(0.0, 0.1, np.shape(self._strength))
        self._strength = np.max([0., self._strength])


class source():

    def __init__(self, strength, xpos, ypos, zpos, weight=1.):
        """
        An actual source, typically defined like a particle.
        """
        self._particle = particle(strength, xpos, ypos, zpos, weight)

    def get_position(self):
        """
        Returns position of the source.
        """
        return self._particle.get_position()

    def get_weight(self):
        """
        Returns weight of the source. This is particularly for a resolved
        source, where weight refers to confidence measure.
        """
        return self._particle.update_weight(1.)


class sensor():

    def __init__(self, xpos, ypos, zpos):
        """
        A sensor is basically described by its position in space and
        efficiency constant. The current model is circularly symmetrical.
        """
        self._position = np.array([xpos, ypos, zpos], dtype=np.float64)
        self._bgrad = np.random.normal(0., 0.005)  # Background Radiaton
        # VERY low. Easily ignorable, but leaving room for outliers
        self._efficiency = 1.

    def get_position(self):
        """
        Returns position of the sensor.
        """
        return self._position

    def read(self):
        """
        Returns the theoretical sensor reading with the given environments
        setup. Can be extended to add a small Poisson noise.
        The readings here are in CPM.
        """
        hits = self._bgrad
        for point in sources:
            hits += point._particle.influence(self._position)

        # Accounting for resolved sources
        for point in known_sources:
            hits -= point._particle.influence(self._position)

        reading = hits * self._efficiency  # Scaling factor for uCi to CPM
        # return reading
        # For simplicity, returning hits only.
        return hits
