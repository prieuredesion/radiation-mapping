#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Localization of multiple radiation sources using sequential 
particle filter and mean-shift techniques in three dimensions

Copyright © 2017 by Dhruv Ilesh Shah
shahdhruv@cmu.edu | dhruv.shah@iitb.ac.in
Robotics Institute, Carnegie Mellon University
Indian Institute of Technology, Bombay
"""

from environment_3d import *
import mean_shift as ms


def visualise_grid(show_sources=False, show_clusters=True, show_resolved=True, show_particles=True):
    """
    Simple Matplotlib-based visualisation for the 3D grid.
    This includes the particles, sources and sensors.
    """
    plt.close()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    # Plotting the particles
    xpos = []
    ypos = []
    zpos = []
    weights = []
    if len(particles) > 0 and show_particles:
        for i in range(len(particles)):
            x, y, z = particles[i].get_position()
            xpos.append(x)
            ypos.append(y)
            zpos.append(z)
            weights.append(particles[i].update_weight(1.))
        weights = np.array(weights, dtype=np.float64) * 80
        ax.scatter(xpos, ypos, zpos, s=weights, c='black',
                   marker='.', label='Particle')
        ax.legend()

    # Similarly plotting the sources and sensors
    if show_sources:
        xpos = []
        ypos = []
        zpos = []
        for i in range(len(sources)):
            x, y, z = sources[i].get_position()
            xpos.append(x)
            ypos.append(y)
            zpos.append(z)
        ax.scatter(xpos, ypos, zpos, s=50, c='blue',
                    marker='^', label='Source')

    xpos = []
    ypos = []
    zpos = []
    for i in range(len(sensors)):
        x, y, z = sensors[i].get_position()
        xpos.append(x)
        ypos.append(y)
        zpos.append(z)
    ax.scatter(xpos, ypos, zpos, s=1, c='magenta',
                marker='D', label='Sensor')

    # Plotting the cluster centroids
    if show_clusters:
        if not len(clusters) == 0:
            xpos = []
            ypos = []
            zpos = []
            for centroid in cluster_centroids:
                x, y, z = centroid.get_position()
                xpos.append(x)
                ypos.append(y)
                zpos.append(z)
            ax.scatter(xpos, ypos, zpos, s=50, c='red',
                        marker='x', label='Cluster Centroid')

    # Plotting known/resolved sources
    if show_resolved and len(known_sources) > 0:
        xpos = []
        ypos = []
        zpos = []
        for known in known_sources:
            x, y, z = known.get_position()
            xpos.append(x)
            ypos.append(y)
            zpos.append(z)
            ax.text(x, y, z, round(known.get_weight(), 3))
        ax.scatter(xpos, ypos, zpos, s=50, c='green',
                    marker='d', label='Resolved Sources')

    ax.legend()
    plt.title(str(len(sources)) + " Source Localization; " + str(particle_population) +
              " Particles; " + str(sensor_count) + " Sensor Network")
    plt.show(block=False)

"""
    plt.close()
    # Plotting the particles
    xpos = []
    ypos = []
    weights = []
    if len(particles) > 0 and show_particles:
        for i in range(len(particles)):
            x, y = particles[i].get_position()
            xpos.append(x)
            ypos.append(y)
            weights.append(particles[i].update_weight(1.))
        weights = np.array(weights, dtype=np.float64) * 80
        plt.scatter(xpos, ypos, s=weights, color='black',
                    marker='.', label='Particle')

    # Similarly plotting the sources and sensors
    if show_sources:
        xpos = []
        ypos = []
        for i in range(len(sources)):
            x, y = sources[i].get_position()
            xpos.append(x)
            ypos.append(y)
        plt.scatter(xpos, ypos, s=100, color='blue',
                    marker='^', label='Source')

    xpos = []
    ypos = []
    for i in range(len(sensors)):
        x, y = sensors[i].get_position()
        xpos.append(x)
        ypos.append(y)
    plt.scatter(xpos, ypos, s=50, color='magenta',
                marker='D', label='Sensor')

    # Plotting the cluster centroids
    if show_clusters:
        if not len(clusters) == 0:
            xpos = []
            ypos = []
            for centroid in cluster_centroids:
                x, y = centroid.get_position()
                xpos.append(x)
                ypos.append(y)
            plt.scatter(xpos, ypos, s=100, color='red',
                        marker='x', label='Cluster Centroid')

    # Plotting known/resolved sources
    if show_resolved and len(known_sources) > 0:
        xpos = []
        ypos = []
        for known in known_sources:
            x, y = known.get_position()
            xpos.append(x)
            ypos.append(y)
            plt.text(x, y, round(known.get_weight(), 3))
        plt.scatter(xpos, ypos, s=100, color='green',
                    marker='d', label='Resolved Sources')

    plt.legend()
    plt.title(str(len(sources)) + " Source Localization; " + str(particle_population) +
              " Particles; " + str(sensor_count) + " Sensor Network")
    plt.show(block=False)
"""


def spawn_sensors(num, mode="traversal"):
    """
    Spawns well-positioned sensors in the grid. There are two modes.
    traversal would give symmetrically placed sensors, whereas
    random would assume uniform IID placement of sensors.
    """
    [length, width, depth] = num

    if mode == "traversal":
        length -= 1
        width -= 1
        for i in range(length + 1):
            xpos = grid_size * i / length
            for j in range(width + 1):
                ypos = grid_size * j / width
                for k in range(depth + 1):
                    zpos = grid_size * k / width
                    sensors.append(sensor(xpos, ypos, zpos))
    elif mode == "random":
        for _ in range(length * width * depth):
            xpos, ypos, zpos = np.random.uniform(
                low=0., high=grid_size - 1, size=3)
            sensors.append(sensor(xpos, ypos, zpos))

    print str(len(sensors)) + " sensors spawned!"


def spawn_sources(num, mode="random", axis="x"):
    """
    Randomly spawns num sensors in the grid if mode is random.
    If mode is wall, creates a wall of closely-spaced num sensors along axis.
    """
    strength = 6.
    if mode == "random":
        for _ in range(num[0]):
            xpos, ypos, zpos = np.random.uniform(
                low=grid_size / 8, high=grid_size * 7 / 8, size=3)
            sources.append(source(strength, xpos, ypos, zpos))
    elif mode == "wall":
        for n in range(num[0]):
            xpos, ypos, zpos = np.random.uniform(
                low=grid_size / 6, high=grid_size * 4 / 6., size=3)
            for i in range(num[1]):
                sources.append(
                    source(strength / (num[1] * num[0]), xpos + i * 0.5, ypos, zpos))
    print str(len(sources)) + " sources spawned!"


def normalise_weights():
    """
    Normalises the weights of the particles so that \sum_i w(p_i) = 1
    """
    sum_of_weights = 0.
    for point in particles:
        sum_of_weights += point.update_weight(1.)
    for point in particles:
        _ = point.update_weight(1 / sum_of_weights)


def spawn_particles(population):
    """
    Particle Initialization Step.
    Generates the particle population drawn IID from a
    uniform distribution. Some knowledge of source magnitude
    is assumed, but that can be compensated.
    """
    for _ in range(population):
        # Each particle assumed to be under 1uCi
        strength = 6.  # For the time being, only playing on position
        # strength = np.random.uniform(low=0., high=12.)
        xpos, ypos, zpos = np.random.uniform(
            low=0., high=grid_size - 1, size=3)
        particles.append(particle(strength, xpos, ypos, zpos, 1. / population))

    print str(len(particles)) + " particles spawned!"


def update_weights(sensor):
    """
    Particle Weighting Step.
    A lot of ambiguity in this part of the implementation, but
    going ahead with single particle per fusion range model. Also
    assumes known and fixed source-particle intensity.
    """
    reading = sensor.read()
    population = len(particles)
    i = 0
    updated_range = []
    range_weights = []
    while i < population:
        # print particles[i].get_distance(sensor.get_position())
        if particles[i].get_distance(sensor.get_position()) < fusion_range:
            # print particles[i].get_position()
            # print reading, particles[i].get_distance(sensor.get_position())
            # Update weight according to Poisson
            expected = particles[i].influence(sensor.get_position())

            factor = (poisson.pmf(floor(reading), expected) +
                      poisson.pmf(ceil(reading), expected)) / 2.
            factor_scaling = np.max([poisson.pmf(
                floor(expected), expected), poisson.pmf(ceil(expected), expected)])
            factor /= factor_scaling
            range_weights.append(particles[i].update_weight(factor))
            updated_range.append(particles[i])
            particles.remove(particles[i])
            i -= 1
            population -= 1
        i += 1
    print "Weights Updated."
    return updated_range, range_weights


def resample_particles(fusion_range, weights):
    """
    Particle Resampling Step.
    The particles are resampled according to their weights in the
    fusion range of the concerned sensor.
    """
    if not len(weights) == 0:
        weights = np.array(weights, dtype=np.float64)
        # This gives a probability distribution
        weights_dist = weights / np.sum(weights)
        sampled_range = np.random.choice(
            fusion_range, len(fusion_range), p=weights_dist)
        for i in range(len(sampled_range)):
            sampled_range[i].add_noise()
            # sampled_range[i]._weight = 1. / \
            #     len(sampled_range) * np.sum(weights)
            sampled_range[i] = deepcopy(sampled_range[i])
        sampled_range = deepcopy(list(sampled_range))
        particles.extend(sampled_range)
    normalise_weights()
    population = len(particles)
    print "Particles Resampled."


def drop_outliers():
    """
    Drops the particles outside the possible range, known a priori.
    An equal number of particles are respawned within the grid, based
    on the current state of the distribution.
    This is under the assumption that there is at least one source
    in the specified range.
    """
    global particle_population
    i = 0
    weights = []

    while i < len(particles):
        xpos, ypos, zpos = particles[i].get_position()
        if (xpos < 0 or xpos > grid_size) or (ypos < 0 or ypos > grid_size):
            particles.remove(particles[i])
            i -= 1
        else:
            weights.append(particles[i].update_weight(1.))
        i += 1
    weights = np.array(weights, dtype=np.float64)
    weights /= np.sum(weights)
    respawned_particles = np.random.choice(
        particles, particle_population - len(particles), p=weights)
    for point in respawned_particles:
        particles.append(deepcopy(point))
    normalise_weights()
    print len(particles)


def cluster(mode="meanshift"):
    """
    Perform clustering of the particles. Indirectly related to declaring
    a particular set of particles as a source (point-approximated).
    """
    global clusters, cluster_centroids

    clusters = []  # Clear existing clusters
    cluster_centroids = []

    if mode == "id":
        # Deprecated. Do not use.
        cluster_set = []
        cluster_ids = []
        for point in particles:
            if point.get_id() not in cluster_ids:
                cluster_ids.append(point.get_id())
            cluster_set.append([point.get_id(), point])
        cluster_set = np.sort(cluster_set, 0)

        for cid in cluster_ids:
            elemental_cluster = []
            for point in cluster_set:
                if point[0] == cid:
                    elemental_cluster.append(point[1])
            clusters.append(elemental_cluster)

    elif mode == "meanshift":
        data = []
        for point in particles:
            strength = point.get_strength()
            [xpos, ypos, zpos] = point.get_position()
            data.append([strength, xpos, ypos, zpos])
        data = np.array(data)
        mean_shifter = ms.MeanShift(kernel='multivariate_gaussian')
        ms_result = mean_shifter.cluster(data, kernel_bandwidth=[1., grid_size / sensor_count[
                                         0] * 0.5, grid_size / sensor_count[1] * 0.5, grid_size / sensor_count[1] * 0.5])
        cluster_ids, indices = np.unique(
            ms_result.cluster_ids, return_index=True)
        for index in indices:
            clusters.append(ms_result.shifted_points[index])

    print str(len(clusters)) + " Clusters Found!"


def confidence_score(mode="meanshift"):
    """
    Returns the confidence score for each cluster detected. This score is irrespective
    of the setup and number of sources/sensors. The score is calculated based on the
    likelihood of sensor readings given the cluster centroid.
    """
    cluster()
    xposns = []
    yposns = []
    confidence = []
    for c in range(len(clusters)):
        if mode == "id":
            cluster_description = []
            for point in clusters[c]:
                cluster_description.append([point.get_position(
                ), point.update_weight(1.), point.get_strength()])
            mean_des = np.mean(cluster_description, 0)
            centroid = particle(mean_des[2], mean_des[0][
                                0], mean_des[0][1], mean_des[1])
        else:
            centroid = particle(clusters[c][0], clusters[
                                c][1], clusters[c][2], clusters[c][3], 1.)
        # Confidence score derived with respect to closest sensor, here
        i_min = 0
        i_min2 = 0
        pos_min = grid_size
        for i in range(len(sensors)):
            if centroid.get_distance(sensors[i].get_position()) < pos_min:
                i_min2 = i_min
                i_min = i
                pos_min = centroid.get_distance(sensors[i].get_position())
        sensor_min = sensors[i_min]
        sensor_min2 = sensors[i_min2]

        expected = centroid.influence(sensor_min.get_position())
        reading = sensor_min.read()
        factor_1 = (poisson.pmf(floor(reading), expected) +
                    poisson.pmf(ceil(reading), expected)) / 2.
        factor_scaling = np.max(poisson.pmf(
            floor(expected), expected), poisson.pmf(ceil(expected), expected))
        factor_1 /= factor_scaling

        expected = centroid.influence(sensor_min2.get_position())
        reading = sensor_min2.read()
        factor_2 = (poisson.pmf(floor(reading), expected) +
                    poisson.pmf(ceil(reading), expected)) / 2.
        factor_scaling = np.max(poisson.pmf(
            floor(expected), expected), poisson.pmf(ceil(expected), expected))
        factor_2 /= factor_scaling
        weighed_factor = factor_1 * 0.6 + factor_2 * 0.4

        cluster_centroids.append(centroid)
        confidence.append(
            [centroid.get_strength(), centroid.get_position(), weighed_factor])
    return confidence


def stage_error(type="mean"):
    """
    Returns the error at current stage, with respect to ground truth.
    The type of error is described by the 'type' argument.
    """
    if mode == "mean":
        pass


def isolate_source():
    """
    One iteration of the modified source localisation algorithm. This
    gets called sequentially till all sources have been localised. Typically, one
    run is expected to localise at least one source - point or bulk. After every
    source localised with confidence greater than confidence_thresh, it is assumed
    to be known and the process continues in a Bayesian fashion.
    """
    global particles

    confidence_thresh = 0.8
    source_thresh = 2.  # Change accordingly
    particles = []
    spawn_particles(particle_population)
    np.random.shuffle(sensors)
    visualise_grid(show_sources=True)

    for i in range(time_steps):
        checksum = 0.
        for sensor in sensors:
            checksum += sensor.read()
            updated_range, range_weights = update_weights(sensor)
            resample_particles(updated_range, range_weights)
        drop_outliers()
        if checksum < bg_thresh:
            print "No more sources to be localised!"
            visualise_grid(show_sources=True)
            break
        print confidence_score()
        visualise_grid(show_sources=True)

    c_text = confidence_score()
    # Get the largest estimate cluster and check accuracy
    # i_max = 0
    # # Only considering the largest magnitude; one source at a time
    # for i in range(len(c_text)):
    #     if c_text[i][0] >= c_text[i_max][0]:
    #         i_max = i
    # plt.text(c_text[i_max][1][0] + 0.5, c_text[i_max][1]
    #          [1], str(int(c_text[i_max][2] * 100)) + "%")
    # plt.show()

    # if c_text[i_max][2] > confidence_thresh:
    #     resolved = c_text[i_max]
    #     known_sources.append(source(resolved[0], resolved[1][
    #                          0], resolved[1][1], weight=c_text[i_max][2]))
    #     print "Isolated a source with %5.2f%% confidence." % (resolved[2] *
    # 100.)

    # Labelling all confident estimates as a source, simultaneously
    for entry in c_text:
        if entry[0] > source_thresh:
            # Shouldn't be required
            # plt.text(entry[1][0] + 0.5, entry[1]
            #          [1], str(int(entry[2] * 100)) + "%")
            if entry[2] > confidence_thresh:
                known_sources.append(source(entry[0], entry[1][
                    0], entry[1][1], entry[1][2], weight=entry[2]))

    visualise_grid(show_sources=True)
    if checksum > bg_thresh:
        isolate_source()


if __name__ == '__main__':
    particle_population = 1000  # Population of the particles
    sensor_count = [5, 5, 5]
    source_count = [1, 1]

    spawn_sources(source_count, mode="wall")
    spawn_sensors(sensor_count, mode="traversal")
    time_steps = 5

    isolate_source()
    visualise_grid(show_sources=True, show_clusters=False,
                   show_resolved=True, show_particles=False)
    plt.show(block=True)

    print "%2.f sources isolated" % (len(known_sources))
    for known in known_sources:
        print known._particle._position
